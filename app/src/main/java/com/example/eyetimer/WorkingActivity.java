package com.example.eyetimer;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import com.airbnb.lottie.LottieAnimationView;

import java.util.concurrent.TimeUnit;

public class WorkingActivity extends AppCompatActivity implements TimePickerDialog.onSelectedTimeInTimePicker {

    long alarmStartTime;
    long timeLeftToAlarmAfterPause;
    long pickedTime;
    boolean timerAlreadyStarted;
    float durationOfAnimation;
    float timeLeftTillAlarm;

    public static final String APP_PREFERENCES = "my_preferences";
    public static final String APP_PREFERENCES_PICKED_TIME = "picked_time";
    public static final String APP_PREFERENCES_ALARM_TIME = "alarm_time";
    public static final String APP_PREFERENCES_TIMER_BOOLEAN = "timer_boolean";

    SharedPreferences timerPreferences;
    ConstraintLayout changeTimeSpanLayout;
    ConstraintLayout stopStartTimerLayout;
    ConstraintLayout resetTimerLayout;
    TextView textStopStarTimer;
    TextView textResetTimer;
    TextView textChangeTimeSpan;
    TextView timerDisplay;
    AlarmManager alarmManager;
    AlarmManager.AlarmClockInfo alarmClockInfo;
    CountDownTimer countDownTimer;
    LottieAnimationView animationView;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.working_activity);
        changeTimeSpanLayout = findViewById(R.id.buttonChangeTimeSpanLayout);
        textChangeTimeSpan = findViewById(R.id.textChangeTimeSpanButton);
        stopStartTimerLayout = findViewById(R.id.buttonStopTimerLayout);
        resetTimerLayout = findViewById(R.id.buttonResetTimerLayout);
        textResetTimer = findViewById(R.id.textResetTimerButton);
        textStopStarTimer = findViewById(R.id.textStopTimerButton);
        timerDisplay = findViewById(R.id.timerDisplay);
        animationView = findViewById(R.id.animation_working_activity);

        timerPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        timerAlreadyStarted = timerPreferences.getBoolean(APP_PREFERENCES_TIMER_BOOLEAN, false);

        changeTimeSpanLayout.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                changeTimeSpanLayout.setBackgroundResource(R.drawable.white_btn_pushed);
                countDownTimer.cancel();
                timerAlreadyStarted = false;
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                changeTimeSpanLayout.setBackgroundResource(R.drawable.white_btn);
                TimePickerDialog tm = new TimePickerDialog();
                FragmentManager fragmentManager = getSupportFragmentManager();
                tm.show(fragmentManager, "dialog");
                deleteAlarm();
                return true;
            }
            return false;
        });

        stopStartTimerLayout.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                stopStartTimerLayout.setBackgroundResource(R.drawable.white_btn_pushed);
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                stopStartTimerLayout.setBackgroundResource(R.drawable.white_btn);
                if (timerAlreadyStarted) {
                    stopTimerBtnMethod();
                } else {
                    startTimerBtnMethod();
                }
                return true;
            }
            return false;
        });
        resetTimerLayout.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                resetTimerLayout.setBackgroundResource(R.drawable.white_btn_pushed);
                if (timerAlreadyStarted) {
                    deleteAlarm();
                    countDownTimer.cancel();
                    timerDisplay.setText(R.string.default_timer);
                    timerAlreadyStarted = false;
                }
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                startTimer(pickedTime);
                setAlarmClock(pickedTime + System.currentTimeMillis());
                alarmStartTime = pickedTime + System.currentTimeMillis();
                timerAlreadyStarted = true;
                return true;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {

        if (timerAlreadyStarted) {
            pickedTime = timerPreferences.getLong(APP_PREFERENCES_PICKED_TIME, 0);
            alarmStartTime = timerPreferences.getLong(APP_PREFERENCES_ALARM_TIME, 0);
            startTimer(alarmStartTime - System.currentTimeMillis());
        }

        if (!timerAlreadyStarted) {
            Bundle getExtrasNewTimer = getIntent().getExtras();
            pickedTime = getExtrasNewTimer.getLong("pickedTime");
            alarmStartTime = System.currentTimeMillis() + pickedTime;
            timerAlreadyStarted = true;
            setAlarmClock(alarmStartTime);
            startTimer(pickedTime);
        }
        super.onStart();
    }

    @Override
    protected void onPause() {
        timerPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = timerPreferences.edit();
        editor.putBoolean(APP_PREFERENCES_TIMER_BOOLEAN, timerAlreadyStarted);
        editor.putLong(APP_PREFERENCES_ALARM_TIME, alarmStartTime);
        editor.putLong(APP_PREFERENCES_PICKED_TIME, pickedTime);
        editor.apply();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (!timerAlreadyStarted) {
            timerPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = timerPreferences.edit();
            editor.putBoolean(APP_PREFERENCES_TIMER_BOOLEAN, false);
            editor.putLong(APP_PREFERENCES_ALARM_TIME, 0);
            editor.putLong(APP_PREFERENCES_PICKED_TIME, 0);
            editor.apply();
        }
        super.onSaveInstanceState(outState);
    }

    private void startTimer(long timeToAlarm) {
        countDownTimer = new CountDownTimer(timeToAlarm, 1000) {
            @Override
            public void onTick(long l) {
                @SuppressLint("DefaultLocale") String timeLeft = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(l),
                        TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(l)),
                        TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));
                timerDisplay.setText(timeLeft);
                timeLeftTillAlarm = (float) l / timeToAlarm;
                durationOfAnimation = 1.0f - timeLeftTillAlarm;
                animationView.reverseAnimationSpeed();
                animationView.setProgress(durationOfAnimation);
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                timerDisplay.setText(R.string.default_timer);
            }
        }.start();
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private PendingIntent getAlarmInfoPendingIntent() {
        Intent alarmInfoIntent = new Intent(this, WorkingActivity.class);
        alarmInfoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(this, 0, alarmInfoIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private PendingIntent getAlarmActionPendingIntent() {
        Intent intent = new Intent(this, AlarmActivity.class);
        intent.putExtra("pickedTime", pickedTime);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void getSelectedTime(long selectedTimeInMillis) {
        pickedTime = selectedTimeInMillis;
        alarmStartTime = selectedTimeInMillis + System.currentTimeMillis();
        timerAlreadyStarted = true;
        startTimer(pickedTime);
        setAlarmClock(alarmStartTime);
        textStopStarTimer.setText(R.string.start_stop_button_stop);
    }

    private void deleteAlarm() {
        alarmManager.cancel(getAlarmActionPendingIntent());
        timerAlreadyStarted = false;
    }

    private void setAlarmClock(long timeWhenAlarmStart) {
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmClockInfo = new AlarmManager.AlarmClockInfo(timeWhenAlarmStart, getAlarmInfoPendingIntent());
        alarmManager.setAlarmClock(alarmClockInfo, getAlarmActionPendingIntent());
    }

    private void stopTimerBtnMethod() {
        textStopStarTimer.setText(R.string.start_stop_button_start);
        countDownTimer.cancel();
        timerAlreadyStarted = false;
        long l = alarmStartTime - System.currentTimeMillis();
        @SuppressLint("DefaultLocale") String timeLeft = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(l),
                TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(l)),
                TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));
        timerDisplay.setText(timeLeft);
        deleteAlarm();
        timeLeftToAlarmAfterPause = l;
        Toast.makeText(this, "Timer stopped", Toast.LENGTH_LONG).show();
    }

    private void startTimerBtnMethod() {
        textStopStarTimer.setText(R.string.start_stop_button_stop);
        startTimer(timeLeftToAlarmAfterPause);
        alarmStartTime = timeLeftToAlarmAfterPause + System.currentTimeMillis();
        timerAlreadyStarted = true;
        setAlarmClock(alarmStartTime);
        Toast.makeText(this, "Timer started", Toast.LENGTH_LONG).show();
    }
}