package com.example.eyetimer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.shawnlin.numberpicker.NumberPicker;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class TimePickerDialog extends DialogFragment {

    public interface onSelectedTimeInTimePicker {
        void getSelectedTime(long selectedTimeInMillis);
    }

    onSelectedTimeInTimePicker selectedTimeInTimePicker;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        try {
            selectedTimeInTimePicker = (onSelectedTimeInTimePicker) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "activity mast implement onSelectedTimeInTimePicker interface");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    int hour;
    int minutes;
    int seconds;
    long selectedTimeInMillis;
    boolean pickerChanged;
    NumberPicker npHour;
    NumberPicker npMinutes;
    NumberPicker npSeconds;
    ConstraintLayout btn;
    String message;

    @Nullable
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Objects.requireNonNull(getDialog()).getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View timePickerDialog = inflater.inflate(R.layout.time_picker_dialog_window, container, false);
        npHour = timePickerDialog.findViewById(R.id.number_picker_hours);
        npMinutes = timePickerDialog.findViewById(R.id.number_picker_minutes);
        npSeconds = timePickerDialog.findViewById(R.id.number_picker_seconds);
        btn = timePickerDialog.findViewById(R.id.set_time_btn_picker_layout);
        pickerChanged = false;
        message = getActivity().getApplicationContext().getString(R.string.please_choose_time);
        btn.setBackgroundResource(R.drawable.set_timer_btn_picker_unfocused);

        setSettingsForTimePiker(npHour, npMinutes, npSeconds);

        npHour.setOnValueChangedListener((picker, oldVal, newVal) -> {
            pickerChanged = true;
            btn.setBackgroundResource(R.drawable.set_timer_btn_picker_focused);
            hour = newVal;
        });

        npMinutes.setOnValueChangedListener((picker, oldVal, newVal) -> {
            pickerChanged = true;
            btn.setBackgroundResource(R.drawable.set_timer_btn_picker_focused);
            minutes = newVal;
        });

        npSeconds.setOnValueChangedListener((picker, oldVal, newVal) -> {
            pickerChanged = true;
            btn.setBackgroundResource(R.drawable.set_timer_btn_picker_focused);
            seconds = newVal;
        });

        btn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                if (pickerChanged) {
                    btn.setBackgroundResource(R.drawable.set_timer_btn_picker_focused_pressed);
                }
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (!pickerChanged) {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    btn.setBackgroundResource(R.drawable.set_timer_btn_picker_focused);
                    selectedTimeInMillis = 1000+TimeUnit.HOURS.toMillis(hour) + TimeUnit.MINUTES.toMillis(minutes) + TimeUnit.SECONDS.toMillis(seconds);
                    selectedTimeInTimePicker.getSelectedTime(selectedTimeInMillis);
                    dismiss();
                }
                return true;
            }
            return false;
        });

        return timePickerDialog;
    }

    private void setSettingsForTimePiker(NumberPicker hourPicker, NumberPicker minutePicker, NumberPicker secondsPicker) {
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(23);
        hourPicker.setValue(0);

        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(59);
        minutePicker.setValue(0);

        secondsPicker.setMinValue(0);
        secondsPicker.setMaxValue(59);
        secondsPicker.setValue(0);
    }
}
