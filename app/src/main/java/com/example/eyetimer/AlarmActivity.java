package com.example.eyetimer;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class AlarmActivity extends AppCompatActivity {

    private static final String APP_PREFERENCES_ALARM_TIME = "alarm_time";
    Ringtone ringtone;
    ConstraintLayout alarmLayoutRestartBtn;
    ConstraintLayout alarmLayoutStopBtn;
    TextView textAlarmLayoutRestartBtn;
    TextView textAlarmLayoutStopBtn;
    long pickedTime;
    long alarmStartTime;
    boolean timerAlreadyWorks;
    boolean timerDeleted;
    AlarmManager alarmManager;
    AlarmManager.AlarmClockInfo alarmClockInfo;
    public static final String APP_PREFERENCES = "my_preferences";
    public static final String APP_PREFERENCES_TIMER_BOOLEAN = "timer_boolean";
    public static final String APP_PREFERENCES_PICKED_TIME = "picked_time";
    SharedPreferences timerPreferences;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        alarmLayoutStopBtn = findViewById(R.id.buttonAlarmStopTimerLayout);
        alarmLayoutRestartBtn = findViewById(R.id.alarmButtonRestartTimerLayout);
        textAlarmLayoutStopBtn = findViewById(R.id.textAlarmStopTimerButton);
        textAlarmLayoutRestartBtn = findViewById(R.id.textAlarmRestartTimerButton);
        Uri notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        ringtone = RingtoneManager.getRingtone(this, notificationUri);
        if (ringtone == null) {
            notificationUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(this, notificationUri);
        }
        if (ringtone != null) {
            ringtone.play();
        }

        alarmLayoutStopBtn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                alarmLayoutStopBtn.setBackgroundResource(R.drawable.white_btn_pushed);
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                alarmLayoutStopBtn.setBackgroundResource(R.drawable.white_btn);
                stopTimer();
                return true;
            }
            return false;
        });
        alarmLayoutRestartBtn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                alarmLayoutRestartBtn.setBackgroundResource(R.drawable.white_btn_pushed);
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                alarmLayoutRestartBtn.setBackgroundResource(R.drawable.white_btn);
                restartTimer(pickedTime);
                Intent intent = new Intent(this, WorkingActivity.class);
                startActivity(intent);
                return true;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {
        timerDeleted = false;
        Bundle getExtras = getIntent().getExtras();
        pickedTime = getExtras.getLong("pickedTime");
        timerPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        super.onStart();
    }

    private void restartTimer(long timeTillAlarm) {
        timerAlreadyWorks = true;
        timerDeleted = false;
        pickedTime = timeTillAlarm;
        alarmStartTime = timeTillAlarm + System.currentTimeMillis();
        saveToSharedPreferences(timerAlreadyWorks, alarmStartTime, pickedTime);
        alarmClockInfo = new AlarmManager.AlarmClockInfo(alarmStartTime, getAlarmInfoPendingIntent());
        alarmManager.setAlarmClock(alarmClockInfo, getAlarmActionPendingIntent());

        if (ringtone != null && ringtone.isPlaying()) {
            ringtone.stop();
        }
        finish();
    }

    private void stopTimer() {
        timerAlreadyWorks = false;
        timerDeleted = true;
        pickedTime = 0;
        alarmStartTime = 0;
        saveToSharedPreferences(timerAlreadyWorks, alarmStartTime, pickedTime);
        if (ringtone != null && ringtone.isPlaying()) {
            ringtone.stop();
        }
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        restartTimer(pickedTime);
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        restartTimer(pickedTime);
        System.out.println("TIMER GOT CHANGED");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        saveToSharedPreferences(timerAlreadyWorks, alarmStartTime, pickedTime);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        saveToSharedPreferences(timerAlreadyWorks, alarmStartTime, pickedTime);
        if (ringtone != null && ringtone.isPlaying()) {
            ringtone.stop();
        }
        super.onDestroy();
    }

    private void saveToSharedPreferences(boolean timerState, long alarmTime, long picked_time) {
        timerPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = timerPreferences.edit();
        editor.putBoolean(APP_PREFERENCES_TIMER_BOOLEAN, timerState);
        editor.putLong(APP_PREFERENCES_ALARM_TIME, alarmTime);
        editor.putLong(APP_PREFERENCES_PICKED_TIME, picked_time);
        editor.apply();
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private PendingIntent getAlarmInfoPendingIntent() {
        Intent alarmInfoIntent = new Intent(this, WorkingActivity.class);
        alarmInfoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(this, 0, alarmInfoIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private PendingIntent getAlarmActionPendingIntent() {
        Intent intent = new Intent(this, AlarmActivity.class);
        intent.putExtra("pickedTime", pickedTime);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}