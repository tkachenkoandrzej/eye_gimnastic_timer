package com.example.eyetimer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import com.airbnb.lottie.LottieAnimationView;

public class StartActivity extends AppCompatActivity implements TimePickerDialog.onSelectedTimeInTimePicker {
    boolean timerAlreadyStarted;
    SharedPreferences timerPreferences;
    ConstraintLayout setTimeBtn;
    LottieAnimationView animationViewStartActivity;

    public static final String APP_PREFERENCES = "my_preferences";
    public static final String APP_PREFERENCES_TIMER_BOOLEAN = "timer_boolean";


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.start_activity);
        setTimeBtn = findViewById(R.id.set_time_period_layout_start_activity);
        animationViewStartActivity = findViewById(R.id.start_activity_animation);
        animationViewStartActivity.setSpeed(0.5f);

        timerPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        setTimeBtn.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                setTimeBtn.setBackgroundResource(R.drawable.set_time_btn_start_activity_action_down);
                animationViewStartActivity.playAnimation();
                return true;
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                setTimeBtn.setBackgroundResource(R.drawable.set_time_btn_start_activity_action_up);
                TimePickerDialog tm = new TimePickerDialog();
                FragmentManager fragmentManager = getSupportFragmentManager();
                tm.show(fragmentManager, "dialog");
                return true;
            }
            return false;
        });
    }

    @Override
    protected void onStart() {
        timerAlreadyStarted = timerPreferences.getBoolean(APP_PREFERENCES_TIMER_BOOLEAN, false);

        if (timerAlreadyStarted) {
            Intent intent = new Intent(this, WorkingActivity.class);
            startActivity(intent);
            finish();
        }
        animationViewStartActivity.playAnimation();
        super.onStart();
    }

    @Override
    public void getSelectedTime(long selectedTimeInMillis) {
        Intent intent = new Intent(this, WorkingActivity.class);
        intent.putExtra("pickedTime", selectedTimeInMillis);
        startActivity(intent);
        animationViewStartActivity.playAnimation();
        finish();
    }
}